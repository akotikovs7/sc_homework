<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css">
    <title>Document</title>
</head>
<body>
<div class="container">
    <form action="/delete" method="POST" >
        <div class="all_product">
            <h2>All Products</h2>
            <a href="/">Add Products</a>
            <button type="submit" class="btn">Apply</button>
        </div>
        <hr>
        <div class="cards">
            <?php foreach ($size_prod as $size) :?>
                    <div class="card" id="">
                        <input type="checkbox" name="del[]" value="<?php echo $size->id?>">
                        <p><?php echo $size->sku?></p>
                        <p><?php echo $size->name?></p>
                        <p>Price: <?php echo $size->price?> $</p>
                        <p>Size: <?php echo $size->size?>MB</p>
                    </div>
            <?php endforeach;?>
            <?php foreach ($weight_prod as $weight) :?>
                    <div class="card">
                        <input type="checkbox" name="del[]" value="<?php echo $weight->id?>">
                        <p><?php echo $weight->sku?></p>
                        <p><?php echo $weight->name?></p>
                        <p>Price: <?php echo $weight->price?> $</p>
                        <p>Weight: <?php echo $weight->weight?> KG</p>
                    </div>
            <?php endforeach?>
            <?php foreach ($dimension_prod as $dimension) :?>
                <div class="card">
                    <input type="checkbox" name="del[]" value="<?php echo $dimension->id?>">
                    <p><?php echo $dimension->sku?></p>
                    <p><?php echo $dimension->name?></p>
                    <p>Price: <?php echo $dimension->price?> $</p>
                    <p>Dimension: <?php echo $dimension->height . 'x' . $dimension->width . 'x' .
                            $dimension->length?></p>
                </div>
            <?php endforeach?>
        </div>
    </form>
</div>
</body>
</html>
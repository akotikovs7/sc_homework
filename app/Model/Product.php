<?php

namespace App\Model;

use Illuminate\Database\Capsule\Manager as Capsule;

class Product
{
    // Global setup for Capsule [Elequent]
    public static function db()
    {
        $capsule  = new Capsule();
        $capsule->addConnection([
            'driver' => '', // mysql
            'host' => '',   // host "127.0.0.1"
            'database' => '', // db_name
            'username' => '', // username
            'password' => '', // password
        ]);

        $capsule->setAsGlobal();
        $capsule->bootEloquent();
        return $capsule;
    }

    // Save products in database
    public static function save()
    {
        $products = self::db();
        $products->bootEloquent();
        if (!empty($_POST))
        {
            Capsule::table('products')->insert([
                'sku' => $_POST['sku'],
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'size' => $_POST['size'],
                'weight' => $_POST['weight'],
                'height' => $_POST['height'],
                'width' => $_POST['width'],
                'length' => $_POST['length']
            ]);
        }
    }

    // Get all products from database
    public static function allProducts()
    {
        $products = self::db();
        $products->bootEloquent();
        $size_prod = Capsule::table('products')->where('size', '<>', 0)->get();
        $weight_prod = Capsule::table('products')->where('weight', '<>', 0)->get();
        $dimension_products = Capsule::table('products')->where('height', '<>', 0)->get();

        $all_products = [$size_prod, $weight_prod, $dimension_products];
        return $all_products;
    }

    // Delete products from database, by one, ore mass-delete
    public static function massProductDelete()
    {
        $delete = self::db();
        $delete->bootEloquent();
        $deleted = $_POST['del'];
        foreach ($deleted as $id)
        {
            Capsule::table('products')->delete($id);
        }
    }
}
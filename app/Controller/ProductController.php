<?php

namespace App\Controller;

use App\Model\Product as ProductDb;
use App\View\View;

class ProductController
{

    // Load index page. Form page to add products
    public function index()
    {
        return View::view('index.php');
    }

    // Save Size products in database
    public function postRequest()
    {
        ProductDb::save();
        return header('location: /');
    }

    // Get all products from database and display in all products page
    public function allProducts()
    {
        $products = ProductDb::allProducts();
        $size_prod = $products[0];
        $weight_prod = $products[1];
        $dimension_prod = $products[2];
        return View::view('all-products.php', [
            'size_prod' => $size_prod,
            'weight_prod' => $weight_prod,
            'dimension_prod' => $dimension_prod
        ]);
    }

    // Mass products delete from database
    public function delete()
    {
        ProductDb::massProductDelete();
        return header('location: /');
    }
}